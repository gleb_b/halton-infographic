$(window).on('load', function(){


var svg = Snap("#infographicSVG"); 
 svg.select('#heartGroup').attr({ opacity:1, pointerEvents:'auto' });

var heartIcon = svg.select('#heartIcon');
var heartShade = svg.select('#heartShade');
var heartText = svg.select('#heartText');
var threeCircles = svg.select('#threeCircles');
var promotionCircle = svg.select('#promotionCircle');
var preventionCircle = svg.select('#preventionCircle');
var treatmentCircle = svg.select('#treatmentCircle');
var threeCirclesText = svg.select('#threeCirclesText');
var circles = svg.select('#circlesLayer');
var inner = svg.select('#innerCircle');
var outer = svg.select('#outerCircle');
var innerText = svg.select('#innerText');
var outerText = svg.select('#outerText');
var communityText = svg.select('#communityText');
var caregiversText = svg.select('#caregiversText');
var workforceText = svg.select('#workforceText');
var heartOut, heartIn, shadeIn, shadeOut;
 
function heartPulseIn() { 
  heartIcon.animate({ 'stroke-width': '60' }, 900,  mina.easeIn  );
  // heartText.animate({ transform: 'matrix(1.15,0.00,0.00,1.15,0,0)' }, 900,  mina.easeIn  );
	heartIn =  setTimeout(function(){heartPulseOut()}, 900);	
};  

function heartPulseOut() { 
  heartIcon.animate({ 'stroke-width': '0' }, 900,  mina.easeIn  );
   heartText.animate({ 'stroke-width': '0' }, 900,  mina.easeIn  );
  heartOut =  setTimeout(function(){heartPulseIn()}, 900);
}; 
 
function heartShadeIn() { 	
	heartShade.attr({opacity:0.5});
  	heartShade.animate({ 'stroke-width': '100', opacity:0  }, 900,  mina.easeIn  );
	shadeIn = setTimeout(function(){heartShadeOut()}, 900);	
};  

function heartShadeOut() { 
  heartShade.animate({ 'stroke-width': '0'  }, 900,  mina.easeIn  );
  shadeOut = setTimeout(function(){ heartShadeIn()}, 900);
}; 

function showThreeCircles() {  

    document.getElementById('heart__intro').style.display = 'none';

   clearTimeout(heartIn); clearTimeout(heartOut);
   clearTimeout(shadeIn); clearTimeout(shadeOut);
   heartText.animate({transform: 'scale(1)' },600);
  heartIcon.animate({transform: 'scale(1)' },600);
  heartShade.animate({transform: 'scale(1)', opacity:0 },200);

  promotionCircle.attr({cy:1100});
  preventionCircle.attr({cy:1100, cx:1117});
  treatmentCircle.attr({cy:1100, cx:1117});
  threeCircles.animate({opacity:1},400);
  $('#heartGroup').css('pointerEvents','none');
  setTimeout(function(){
  		promotionCircle.animate({cy: 982},500);
  		preventionCircle.animate({cy: 982},500);
  		treatmentCircle.animate({cy: 982},500);
  }, 400);
  setTimeout(function(){ 
  		preventionCircle.animate({cx:1271, cy: 1239},600);
  		treatmentCircle.animate({cx:1271, cy: 1239},600);
  }, 1100);
  setTimeout(function(){  
  		treatmentCircle.animate({cx:962 },600);
  }, 1800);
  setTimeout(function(){  
  		threeCirclesText.animate({opacity:1 },600);
  }, 2400);
  setTimeout(function(){  
  		inner.attr({r:0 });
  		outer.attr({r:0 });
  		circles.attr({opacity:1 });
  }, 3200);
  setTimeout(function(){   
  		inner.animate({r:490 }, 800);
  }, 3200);
  setTimeout(function(){   
  		innerText.animate({opacity:1 }, 400);  		
  }, 4000);
  setTimeout(function(){   
  		caregiversText.animate({opacity:1 }, 400);
  }, 4400);
  setTimeout(function(){   
  		workforceText.animate({opacity:1}, 400);
  }, 4800);  
  setTimeout(function(){   
  		communityText.animate({opacity:1}, 400);
  }, 5200); 
  setTimeout(function(){   
  		outer.animate({r:680}, 800);
  }, 5700); 
  setTimeout(function(){   
  		outerText.animate({opacity:1}, 400);
  }, 6500);
  setTimeout(function(){   
  		svg.select('#brainIcon').animate({opacity:1}, 400);
  		svg.select('#resilienceIcon').animate({opacity:1}, 400);
  }, 7000);
  setTimeout(function(){   
  		svg.select('#stressIcon').animate({opacity:1}, 400);
  		svg.select('#attachmentIcon').animate({opacity:1}, 400);
  }, 7400);
  setTimeout(function(){   
  		svg.select('#playIcon').animate({opacity:1}, 400);
  		svg.select('#caregivingIcon').animate({opacity:1}, 400);
  }, 7800);
  setTimeout(function(){   
  		svg.select('#executiveIcon').animate({opacity:1}, 400);
  		svg.select('#temperamentIcon').animate({opacity:1}, 400);
      $('.icon-button').css('pointer-events','auto');
      $('#circlesLayer').css('pointer-events','auto');
      $('#innerCircle').css('pointer-events','auto');
      $('#communityText').css('pointer-events','auto');
      $('#caregiversText').css('pointer-events','auto');
  		$('#workforceText').css('pointer-events','auto');
  }, 8200);

  
}; 


 

 heartPulseIn(); heartShadeIn();

 $('#heartGroup').on('click', function(){showThreeCircles()});



function getSVGwidth() {
	var el   = document.getElementById("outerCircle");  
	var rect = el.getBoundingClientRect();  
	return rect.width ;
}

$('.svg-container').css('padding-bottom',getSVGwidth()/5+'px'); 
$('.info-window').css('max-width',getSVGwidth() +'px'); 

$('.icon-button').click(function(){
	var messageID = this.id.replace('Icon','')+'Window'; 

	$('#'+messageID).addClass('active');

}) 

$(document).on('mouseup','body',function(e) 
{
    var container = $('.info-window.active'); 
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.removeClass('active');
    }
});


})

$(window).on('resize', function(){ 
		$('.info-window.active').removeClass('active');
 
});


$('.text-button').click(function(){
  var messageID = this.id.replace('Text','')+'Window'; 

  $('#'+messageID).addClass('active');

}) 